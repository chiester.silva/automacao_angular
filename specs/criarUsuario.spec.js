
const Helper = require('protractor-helper');
const criarUsuario = require('../page_objects/criarUsuario.po');
const { browser } = require('protractor');

describe('Criar Usuário', () => {

    beforeEach(() => criarUsuario.visit())
    afterEach(() => browser.driver.manage().deleteAllCookies())

    it('Cadastro com sucesso informandos todos os dados obrigatórios', () => {       
        criarUsuario.cadastrarUsuario("Chiester", "Silva", "chiester@gmail.com")

        Helper.waitForElementVisibility(criarUsuario.messagemUsuarioCriadoComSucesso);
    })

    it('Cadastro sem preencher o email', () => {
        criarUsuario.cadastrarUsuario("Chiester", "Silva", "")

        Helper.waitForElementNotToBeVisible(criarUsuario.messagemUsuarioCriadoComSucesso);
    })

})

