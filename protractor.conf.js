const SpecReporter = require('jasmine-spec-reporter').SpecReporter;
const AllureReporter = require('jasmine-allure-reporter');

module.exports.config = {
    capabitilities: {
        specs: ['./e2e/**/*.e2e-spec.ts'],
        browserName: 'chrome',
        chromeOptions: {
        args: [ '--headless', '--no-sandbox', '--disable-gpu', '--window-size=1920,1080' ]
        }
    },
    maxSessions: 2,
    directConnect: true,
    baseUrl: 'https://automacaocombatista.herokuapp.com',
    
    onPrepare: () => {
        browser.ignoreSynchronization = true,
        jasmine.getEnv().addReporter(new SpecReporter({
            suite: {
                displayNumber: true
            },
            spec: {
                displayFailed: true,
                displayPending: true,
                displayDuration: true,
                displayStackTrace: true
            },
            summary: {
                displayFailed: true
            }
        }));
        jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
        jasmine.getEnv().addReporter(new AllureReporter());
        jasmine.getEnv().afterEach(function(done){
          browser.takeScreenshot().then(function (png) {
              allure.createAttachment('Screenshot', function () {
                  return new Buffer(png, 'base64')
                },'image/png')();
                done();
            })
        });
    },
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000,
        random: true,
        print: function() {}
    }
}