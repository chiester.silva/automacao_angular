const Helper = require('protractor-helper');

class CriarUsuario{
    constructor() {
        this.nomeInput = element(by.id("user_name"));
        this.ultimoNomeInput = element(by.id("user_lastname"));
        this.emailInput = element(by.id("user_email"));
        this.criarButton = element(by.name("commit"));
        this.messagemUsuarioCriadoComSucesso = element(by.cssContainingText("#notice", "Usuário Criado com sucesso"));
    }

    visit(){
        browser.get("/users/new");
    }

    cadastrarUsuario(nome, ultimoNome, email){
        Helper.fillFieldWithText(this.nomeInput, nome)
        Helper.fillFieldWithText(this.ultimoNomeInput, ultimoNome)
        Helper.fillFieldWithText(this.emailInput, email)
        Helper.click(this.criarButton);
    }
}

module.exports = new CriarUsuario();